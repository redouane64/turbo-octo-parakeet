# Turbo-octo-parakeet

Green API task implemented using NestJS and its built-in logging. In this implementation `m1` takes a query string parameter and pass it to `m2` to convert it to base64. Result is returned to the client.

## Requirements

- NodeJS (check `.nvmrc` for preferred version).

- Docker to run RabbitMQ instance.

## Installation

- Install dependencies for each service `m1` and `m2` via `npm install`.

- Run RabbitMQ docker image via `docker compose up -d`.

- Start `m1` and `m2` via `npm run start` or `npm run start:dev`

## Testing

- Call `m1` service using curl command:

```bash
curl -v -X POST http://localhost:3001/?data=hello-world
```

Example response:

```json
{"original":"hello-world","reply":"aGVsbG8td29ybGQ="}
```
