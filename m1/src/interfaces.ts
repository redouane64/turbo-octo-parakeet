// NOTE: these type duplicated in both service. in real world it
// is recommended to move it in a common contracts package
export interface IJob {
  data: string;
}

export interface IJobReply {
  request: string;
  reply: string;
}
