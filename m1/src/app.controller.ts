import { Controller, Inject, Post, Query } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom, timeout } from 'rxjs';
import { IJob, IJobReply } from './interfaces';

@Controller()
export class AppController {
  constructor(@Inject('JOBS_SERVICE') private client: ClientProxy) {}

  @Post()
  async createJob(@Query('data') data: string) {
    const reply = await firstValueFrom(
      this.client
        .send<IJobReply, IJob>('JOB', {
          data,
        })
        .pipe(timeout(1000)),
    );

    return reply;
  }
}
