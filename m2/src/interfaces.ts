export interface IJob {
  data: string;
}

export interface IJobReply {
  request: string;
  reply: string;
}
