import { Controller, Logger } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext, Transport } from '@nestjs/microservices';
import { Observable, from, of } from 'rxjs';
import { IJob } from './interfaces';

@Controller()
export class AppController {

  @MessagePattern('JOB', Transport.RMQ)
  handleJob(@Payload() payload: IJob, @Ctx() context: RmqContext): Observable<any> {
    return of({ original: payload.data, reply: Buffer.from(payload.data).toString('base64') });
  }
}
